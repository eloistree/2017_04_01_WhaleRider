﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWaleWithTransform : MonoBehaviour {


	public PiloteWale _wale;

	public Transform _waleRoot;
	public Rigidbody _waleRigidBody;

	public float _forwardSpeed=1f;
	public float lateralSpeed=1f;
	public float _horizontalRotation=90;

	public Vector3 wantedVelocity= Vector3.forward;

	public void Update(){
		wantedVelocity = (_waleRoot.forward * _forwardSpeed * _wale.GetSpeedFactor() +_waleRoot.right*_wale._direction.x * lateralSpeed) ;
		_waleRigidBody.velocity = Vector3.Lerp ( _waleRigidBody.velocity, wantedVelocity, Time.deltaTime); 
		_waleRoot.Rotate (new Vector3 (0, _horizontalRotation*_wale._direction.x, 0)*Time.deltaTime);
	}




}
