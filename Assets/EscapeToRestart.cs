﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeToRestart : MonoBehaviour {

	public InputController _controller;

	void Update () {

		if (Input.GetKey (KeyCode.Escape) || _controller.left.menuDown || _controller.right.menuDown)
			Application.LoadLevel (0);
	}
}
