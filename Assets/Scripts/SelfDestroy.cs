﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour {

    [SerializeField]
    private float lifeTime;

	// Use this for initialization
	void Start () {
        StartCoroutine(Suicide());	
	}

    private IEnumerator Suicide()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }
}
